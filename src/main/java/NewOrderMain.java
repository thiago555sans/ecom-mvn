import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.protocol.types.Field;

import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class NewOrderMain {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Foi");
        //Para poder produzir uma mensagem eu preciso de chave  e valor
        var producer = new KafkaProducer<String,String>( properties());
        for (int i = 0; i < 100 ; i++) {
            var key = UUID.randomUUID().toString();
            int produto = (int)(Math.random()*1000);
            float valor = (float) (Math.random()*1000);
            //id do pedido, usuário, valor da compra
            var value = Integer.toString(produto)  +","+ key +","+ Float.toString(valor);
            //realizando a construção de uma mensagem um registro para o cluster kafka
            var record = new ProducerRecord<String, String>("ECOMMERCE_NEW_ORDER", value, value);

            //Enviando a mensagem, aguardado recebimento, e colocando um observer para avisar quando finalizar a transmissão da mensagem
            producer.send(record, getCallback()).get();
            var email = "Obrigado pelo pedido, ele já está sendo processado!";
            var emailRecord = new ProducerRecord<>("ECOMMERCE_SEND_EMAIL", email, email);
            producer.send(emailRecord, getCallback()).get();
        };
    }

    private static Callback getCallback() {
        return (data,ex)->{
            if (ex != null) {
                ex.printStackTrace();
                return;
            }
            System.out.println(data.topic() + " :: " + data.partition() + " / "+ data.offset() + " - "+ data.timestamp());
        };
    }

    private static Properties properties() {
        var properties = new Properties();
        //Setando o servidor do zookeper
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        //Tranformando as strings recebidas em bytes para a chave
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //Tranformando as strings recebidas em bytes para a messagem
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //definindo a quantidade de dados que o kafka vai receber para poder commitar
        properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,"1");
        return properties;
    }
}
