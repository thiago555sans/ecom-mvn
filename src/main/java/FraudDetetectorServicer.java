import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import static java.lang.Thread.*;

public class FraudDetetectorServicer {
    public static void main(String[] args) {
        //Criando consumidor
        var consumer = new KafkaConsumer<String,String>(properties());
        //Informando o tópico que o consumidor vai estar escrito
        consumer.subscribe(Collections.singletonList("ECOMMERCE_NEW_ORDER"));
        //chamada do pool pr que ele fique escutando
        while (true) {
            //Instruindo ao consumer verifcar se existe alguma mensagem por um tempo determinado
            var records = consumer.poll(Duration.ofMillis(100));

            if (!records.isEmpty()) {
                System.out.println("Registro encontrado ID:"+ records.count() );
                for (var record : records) {
                    System.out.println("Processando pedido, verificando a existencia de fraude");
                    System.out.println("Key: "+record.key());
                    System.out.println("Value: "+record.value());
                    System.out.println("Partition: "+record.partition());
                    System.out.println("Record: "+record.offset());
                    try {
                        sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Pedido processado");
                }
            }

        }

    }

    private static Properties properties() {
        var properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,FraudDetetectorServicer.class.getSimpleName());
        return properties;
    }


}
