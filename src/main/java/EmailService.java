import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import static java.lang.Thread.sleep;

public class EmailService {
    public static void main(String[] args) {
        //Criando consumidor
        var consumer = new KafkaConsumer<String,String>(properties());
        //Informando o tópico que o consumidor vai estar escrito
        consumer.subscribe(Collections.singletonList("ECOMMERCE_SEND_EMAIL"));
        //chamada do pool pr que ele fique escutando
        while (true) {
            //Instruindo ao consumer verifcar se existe alguma mensagem por um tempo determinado
            var records = consumer.poll(Duration.ofMillis(100));

            if (!records.isEmpty()) {
                System.out.println(records.count() + "Registro encontrado \n __________________________________________"  );
                for (var record : records) {
                    System.out.println("Enviando email");
                    System.out.println("Key: "+record.key());
                    System.out.println("Value: "+record.value());
                    System.out.println("Partition: "+record.partition());
                    System.out.println("Record: "+record.offset());
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Email Enviado processado");
                }
            }

        }

    }

    private static Properties properties() {
        var properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, EmailService.class.getSimpleName());
        return properties;
    }


}
